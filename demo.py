from stringgen import StringGen


class Demo:
    def __init__(self):
        self.female_name = StringGen('resources/firstnames_female_en.json')
        self.male_name = StringGen('resources/firstnames_male_en.json')
        self.lastname = StringGen('resources/lastnames_en.json')

        self.message = """
            You're trying StringGen.
            This demo show you how StrinGen can generate a random name.
            Please enter '1' or '2' to generate either a male or female name.
            Enter '3' to see this message again.
            Enter '4' to exit the program.\n
        """

        self.commands = {
            "1": {"description": "Generate a female name",
                  "function": self.display_female},
            "2": {"description": "Generate a male name",
                  "function": self.display_male},
            "3": {"description": "View help", "function": self.display_help},
            "4": {"description": "Quit", "function": self.quit}
            }

    def display_female(self):
        print('\n== {} {} ==\n'.format(self.female_name.gen().title(),
                                       self.lastname.gen().upper()))

    def display_male(self):
        print('\n== {} {} ==\n'.format(self.male_name.gen().title(),
                                       self.lastname.gen().upper()))

    def display_help(self):
        print(self.message)

    def display_commands(self):
        for number, command in self.commands.items():
            print("{}: {}".format(number, command["description"]))

    def quit(self):
        return "QUIT"

    def run(self):
        """ runs a console mode demo """

        com = ''
        self.display_help()

        while com != "QUIT":
            self.display_commands()
            com = input('\n>').lower()

            if com in self.commands:
                com = self.commands[com]["function"]()
            else:
                print("\nWrong entry! Enter '3' if you need help.\n")

        print("\nthank you for tried StringGen. Cya.\n")


if __name__ == '__main__':
    Demo().run()
